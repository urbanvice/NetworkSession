//
//  EJUtils+TxtUtils.m
//  NetworkSession
//
//  Created by zhangqiang on 16/3/9.
//  Copyright © 2016年 zhangqiang. All rights reserved.
//

#import "EJUtils+TxtUtils.h"

static char dicTxtKey;

@implementation EJUtils (TxtUtils)

- (void)setDicTxt:(NSDictionary *)dicTxt    {
    objc_setAssociatedObject(self, &dicTxtKey, dicTxt, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSDictionary *)dicTxt  {
    return objc_getAssociatedObject(self, &dicTxtKey);
}

+ (NSString *)txtWithId:(NSInteger)txtId    {
    [[self shareInstance] loadDictTxt];
    
    //while plist deserialized, dictionary key default class is NSString *, use NSNumber * as key to get value is unavailable
    NSString * txt = [[[self shareInstance] dicTxt] objectForKey:[NSString stringWithFormat:@"%ld", txtId]];
    NSLog(@"txt with id: %ld, string: %@", (long)txtId, txt);
    
    return txt;
}

- (void)loadDictTxt {
    if ([self dicTxt] != nil) {
        return;
    }
    
    /*
     * NSHomeDirectory is for sandbox file path, not bundle file path, bundle is just subset of sandbox
     * txt.plist is a bundle file
    */
//    NSString *filePath = [NSHomeDirectory() stringByAppendingString:@"/txt.plist"];
//    NSLog(@"txt file path: %@", filePath);
    
    NSString *file = [[NSBundle mainBundle] pathForResource:@"txt.plist" ofType:nil];
    NSLog(@"bundle txt file path: %@", file);
    self.dicTxt = [NSDictionary dictionaryWithContentsOfFile:file];
    NSLog(@"dicTxt content: %@", self.dicTxt);
}

@end
