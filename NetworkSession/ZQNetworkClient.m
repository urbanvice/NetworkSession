//
//  ZQNetworkClient.m
//  NetworkSession
//
//  Created by zhangqiang on 16/3/10.
//  Copyright © 2016年 zhangqiang. All rights reserved.
//

#import "ZQNetworkClient.h"

@implementation ZQNetworkClient
@synthesize clientSession, downlodTaskPool;

+ (instancetype)sharedInstance {
    static id client = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        client = [[self alloc ] init];
    });
    
    return client;
}

- (instancetype)init    {
    self = [super init];
    if (self != nil) {
        clientSession = [self backgroundURLSession];
        downlodTaskPool = [[NSMutableDictionary alloc] init];
    }
    
    return self;
}


- (NSURLSession *)backgroundURLSession {
    NSString *identifer = MAIN_VIEW_SESSION_IDENTIFIER;
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:identifer];
    config.discretionary = YES;
    config.allowsCellularAccess = YES;
    
//    config.TLSMaximumSupportedProtocol = kDTLSProtocol1;
    
    //    NSURLSessionDownloadTask *download = [session downloadTaskWithRequest:request completionHandler:^(NSURL * _Nullable location, NSURLResponse * _Nullable response, NSError * _Nullable error) {
    //        NSLog(@"download success");
    //    }];
    NSURLSession *backgroundSession = [NSURLSession sessionWithConfiguration:config delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    
    return backgroundSession;
}

- (void)startDownloadTaskWithURLString:(NSString *)url {
    ZQNetworkObject *obj = [downlodTaskPool objectForKey:url];
    if (obj == nil) {
        obj = [[ZQNetworkObject alloc] init];
        obj.sUrl = url;
        [downlodTaskPool setObject:obj forKey:url];
    }
    
    if (obj.downloadTask != nil) {
//        [obj.downloadTask resume];
        return;
    }
    
    NSData *resumeData = [[NSUserDefaults standardUserDefaults] objectForKey:url];
    NSURLSessionDownloadTask *downloadTask = nil;
    if (resumeData != nil) {
        downloadTask = [clientSession downloadTaskWithResumeData:resumeData];
    }   else    {
        NSURL *downloadUrl = [NSURL URLWithString:url];
        NSURLRequest *request = [NSURLRequest requestWithURL:downloadUrl];
        downloadTask = [clientSession downloadTaskWithRequest:request];
    }
    
    obj.downloadTask = downloadTask;
    
    [downloadTask resume];
}

- (void)cancelDownloadTaskWithURLString:(NSString *)url     {
    ZQNetworkObject *obj = [downlodTaskPool objectForKey:url];
    if (obj == nil || (obj != nil && obj.downloadTask == nil)) {
        return;
    }
    
    __weak ZQNetworkObject * weakObj = obj;
//    __weak NSMutableDictionary * weakDownloaTaskPool = downlodTaskPool;
    [obj.downloadTask cancelByProducingResumeData:^(NSData * _Nullable resumeData) {
//        weakObj.resumeData = resumeData;
        [[NSUserDefaults standardUserDefaults] setObject:resumeData forKey:url];
        [[NSUserDefaults standardUserDefaults] synchronize];
        weakObj.downloadTask = nil;
    }];
}


#pragma mark - session delegate

- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge
 completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential * __nullable credential))completionHandler   {
    NSLog(@"session challenge occurs");
    completionHandler(NSURLSessionAuthChallengeUseCredential, nil);
}

- (void)URLSession:(NSURLSession *)session didBecomeInvalidWithError:(nullable NSError *)error {
    NSLog(@"session : %@, error: %@", session, error);
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error
{
    NSLog(@" url = %@, error  = %@", [[[task originalRequest] URL] absoluteString], [error localizedDescription]);
}

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveResponse:(NSURLResponse *)response completionHandler:(void (^)(NSURLSessionResponseDisposition))completionHandler {
    NSLog(@"response allow");

//    completionHandler(NSURLSessionResponseAllow);
}



- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask
didFinishDownloadingToURL:(NSURL *)location {
    NSLog(@"did finish download, localtion: %@", [location absoluteString]);
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask
      didWriteData:(int64_t)bytesWritten
 totalBytesWritten:(int64_t)totalBytesWritten
totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite    {
    NSLog(@"download url: %@", [[[downloadTask currentRequest] URL] absoluteString]);
    
    NSDictionary *info = @{@"session":session,
                           @"downloadTask":downloadTask,
                           @"didWriteData":[NSNumber numberWithLongLong:bytesWritten],
                           @"totalBytesWritten":[NSNumber numberWithLongLong:totalBytesWritten],
                           @"totalBytesExpectedToWrite":[NSNumber numberWithLongLong:totalBytesExpectedToWrite]};
    NSLog(@"info: %@", info);
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:NC_DOWNLOAD_PROGRESS object:self userInfo:info];
}


@end
