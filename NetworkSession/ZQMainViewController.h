//
//  ZQMainViewController.h
//  NetworkSession
//
//  Created by zhangqiang on 16/3/9.
//  Copyright © 2016年 zhangqiang. All rights reserved.
//

//#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, ZQMainViewButtonTag) {
    ZQMainViewButtonTagStart = 134,
    ZQMainViewButtonTagStop,
    ZQMainViewButtonTagResume,
};

@interface ZQMainViewController : UIViewController  {
    __weak UITextField * tfUrl;
    __weak UIProgressView * pvDownload;
    __weak UILabel * lbDownload;
    
}

@property (nonatomic, weak) UITextField *tfUrl;
@property (nonatomic, weak) UIProgressView *pvDownload;
@property (nonatomic, weak) UILabel * lbDownload;


/*
 * touch start button up inside
 */
- (void)touchStartUpInside:(id)sender forEvent:(UIEvent *)event;

/*
 * touch stop button up inside
 */
- (void)touchStopUpInside:(id)sender forEvent:(UIEvent *)event;

/*
 * touch resume button up inside
 */
- (void)touchResumeInside:(id)sender forEvent:(UIEvent *)event;

- (void)dealwithNotification:(NSNotification *)notification;

/*
 * deal with download notifictaion
 */
- (void)downloadProgress:(NSNotification *)notification;

@end
