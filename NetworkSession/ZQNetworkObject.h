//
//  NSNetworkObject.h
//  NetworkSession
//
//  Created by zhangqiang on 16/3/14.
//  Copyright © 2016年 zhangqiang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZQNetworkObject : NSObject   {
    NSString * sUrl;
    NSData * resumeData;
    NSURLSessionDownloadTask * downloadTask;
}

@property (nonatomic, copy) NSString *sUrl;
@property (nonatomic, strong) NSData *resumeData;
@property (nonatomic, strong) NSURLSessionDownloadTask * downloadTask;

@end
