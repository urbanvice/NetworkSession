//
//  ZQMainViewController.m
//  NetworkSession
//
//  Created by zhangqiang on 16/3/9.
//  Copyright © 2016年 zhangqiang. All rights reserved.
//

#import "ZQMainViewController.h"

@interface ZQMainViewController ()

@end

@implementation ZQMainViewController
@synthesize tfUrl, pvDownload;

- (void)viewWillAppear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(downloadProgress:) name:NC_DOWNLOAD_PROGRESS object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NC_DOWNLOAD_PROGRESS object:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    // url input text field
    CGRect textFieldFrame = CGRectMake((SCREENWIDTH-300)*0.5, 100, 300, 30);
    UITextField *textField = [[UITextField alloc] initWithFrame:textFieldFrame];
    textField.borderStyle = UITextBorderStyleRoundedRect;
    textField.placeholder = [EJUtils txtWithId:1];
    [self.view addSubview:textField];
    self.tfUrl = textField;
    
    //start/stop/resume download button
    NSInteger tags[3] = {ZQMainViewButtonTagStart, ZQMainViewButtonTagStop, ZQMainViewButtonTagResume};
    SEL       sels[3] = {@selector(touchStartUpInside:forEvent:), @selector(touchStopUpInside:forEvent:), @selector(touchResumeInside:forEvent:)};
    for (int i = 0; i < 3; i++) {
        CGRect btnStartFrame = CGRectMake(textFieldFrame.origin.x + 65*i, textFieldFrame.origin.y + 40, 60, 30);
        UIButton *btnStart = [UIButton buttonWithNormalColor:[UIColor blueColor] SelectedColor:[UIColor redColor] frame:btnStartFrame];
        btnStart.tag = tags[i];
        btnStart.showsTouchWhenHighlighted = YES;
    //    btnStart.reversesTitleShadowWhenHighlighted = NO;
        btnStart.tintColor = [UIColor greenColor];
        [btnStart setTitle:[EJUtils txtWithId:2+i] forState:UIControlStateNormal];
        [btnStart setTitle:[EJUtils txtWithId:2+i] forState:UIControlStateHighlighted];
        [self.view addSubview:btnStart];
        
        [btnStart addTarget:self action:sels[i] forControlEvents:UIControlEventTouchUpInside];
    }
    
    //progress bar
    UIProgressView *progress = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleDefault];
    CGRect progressFrame = CGRectMake(textFieldFrame.origin.x, textFieldFrame.origin.y + 80, 300, 20);
    progress.frame = progressFrame;
    progress.transform = CGAffineTransformMakeScale(1.0f,3.0f);  //scale height
    progress.progress = 0;
    [self.view addSubview:progress];
    self.pvDownload = progress;
    
    
    //progress label
    UILabel *progressLabel = [[UILabel alloc] initWithFrame:CGRectMake(textFieldFrame.origin.x + 100, textFieldFrame.origin.y + 90, 100, 20)];
    progressLabel.backgroundColor = [UIColor greenColor];
    progressLabel.text = @"00.00%";
    [self.view addSubview:progressLabel];
    self.lbDownload = progressLabel;
}

- (BOOL)isURLAvailable  {
    BOOL bRet = NO;
    if (self.tfUrl.text == nil || self.tfUrl.text.length == 0) {
        [UIAlertController alertTarget:self withTitle:@"Alert" message:@"Please input url!"];
        return bRet;
    }
    
    bRet = YES;
    return bRet;
}

#pragma mark - method called for buttons

- (void)touchStartUpInside:(id)sender forEvent:(UIEvent *)event {
    NSLog(@"touch start up inside");
//    self.tfUrl.text = @"http://img2.3lian.com/2014/f5/158/d/86.jpg";
    self.tfUrl.text = @"http://res.taig.com/installer/mac/TaiGjailbreak_V110.dmg";
    
    if ([self isURLAvailable] == NO) {
        return;
    }
    
    [NETWORKCLIENT startDownloadTaskWithURLString:self.tfUrl.text];
}

- (void)touchStopUpInside:(id)sender forEvent:(UIEvent *)event {
    NSLog(@"touch stop up inside");
    
    if ([self isURLAvailable] == NO) {
        return;
    }

    [NETWORKCLIENT cancelDownloadTaskWithURLString:self.tfUrl.text];
}

- (void)touchResumeInside:(id)sender forEvent:(UIEvent *)event {
    NSLog(@"touch resume up inside");
    [self touchStartUpInside:sender forEvent:event];
}

- (void)dealwithNotification:(NSNotification *)notification {
    NSString * name = [notification name];
    if ([name isEqualToString:NC_DOWNLOAD_PROGRESS])    {
        [self downloadProgress:notification];
    }
}

- (void)downloadProgress:(NSNotification *)notification  {
    NSDictionary *info = [notification userInfo];
    int64_t didWriteData = [[info objectForKey:@"didWriteData"] longLongValue];
    int64_t totalBytesWritten = [[info objectForKey:@"totalBytesWritten"] longLongValue];
    int64_t totalBytesExpectedToWrite = [[info objectForKey:@"totalBytesExpectedToWrite"] longLongValue];
    
    if (totalBytesExpectedToWrite == NSURLSessionTransferSizeUnknown) {
        [UIAlertController alertTarget:self withTitle:@"Alert" message:@"totalBytesExpectedToWrite is NSURLSessionTransferSizeUnknown"];
        self.pvDownload.progress = 0;
        return;
    }
    
    NSLog(@"totalBytesWritten: %lld, tottalBytesExpectedToWrite: %lld", totalBytesWritten, totalBytesExpectedToWrite);
    self.pvDownload.progress = (float)totalBytesWritten / totalBytesExpectedToWrite;
    NSLog(@"self.pvDownload.progress ===== %f", self.pvDownload.progress);
    
    self.lbDownload.text = [NSString stringWithFormat:@"%.2f%%", (self.pvDownload.progress * 100)];
    
 }

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
