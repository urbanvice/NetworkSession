//
//  EJUtils+TxtUtils.h
//  NetworkSession
//
//  Created by zhangqiang on 16/3/9.
//  Copyright © 2016年 zhangqiang. All rights reserved.
//

#import "EJUtils.h"

@interface EJUtils (TxtUtils)

@property (nonatomic, retain) NSDictionary<NSString *, NSString *>* dicTxt; //txt dictionary

+ (NSString *)txtWithId:(NSInteger)txtId;

- (void)loadDictTxt;
@end
