//
//  ZQNetworkClient.h
//  NetworkSession
//
//  Created by zhangqiang on 16/3/10.
//  Copyright © 2016年 zhangqiang. All rights reserved.
//

//#import <Foundation/Foundation.h>

@interface ZQNetworkClient : NSObject <NSURLSessionTaskDelegate, NSURLSessionDataDelegate, NSURLSessionDownloadDelegate>  {
    NSURLSession * clientSession;
    NSMutableDictionary *downlodTaskPool;
}

@property (nonatomic, strong, readonly) NSURLSession *clientSession;
@property (nonatomic, strong, readonly) NSMutableDictionary *downlodTaskPool;

+ (instancetype)sharedInstance;

- (NSURLSession *)backgroundURLSession;

- (void)startDownloadTaskWithURLString:(NSString *)url;

- (void)cancelDownloadTaskWithURLString:(NSString *)url;

@end
