//
//  EJUtils.h
//  easyjob
//
//  Created by zhangqiang on 16/1/25.
//  Copyright © 2016年 zhangqiang. All rights reserved.
//

#import <Foundation/Foundation.h>
@class EJUtils;

static EJUtils *g_instance = nil;

@interface EJUtils : NSObject
+ (instancetype)shareInstance;


+ (UIImage *)imageWithColor:(UIColor *)color AndSize:(CGSize)size AndCornerRadius:(CGFloat)radius;
+ (NSObject *)deepCopy:(NSObject<NSCoding> *)object;
@end
