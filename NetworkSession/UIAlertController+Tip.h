//
//  UIAlertController+Tip.h
//  NetworkSession
//
//  Created by zhangqiang on 16/3/10.
//  Copyright © 2016年 zhangqiang. All rights reserved.
//

//#import <UIKit/UIKit.h>

@interface UIAlertController (Tip)
+ (void)alertTarget:(UIViewController *)target withTitle:(NSString *)title message:(NSString *)msg;
@end
