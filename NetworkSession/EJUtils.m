//
//  EJUtils.m
//  easyjob
//
//  Created by zhangqiang on 16/1/25.
//  Copyright © 2016年 zhangqiang. All rights reserved.
//

#import "EJUtils.h"

@implementation EJUtils

+ (instancetype)shareInstance {
    if (!g_instance) {
        g_instance = [[self alloc] init];
    }

    return g_instance;
}

//+ (instancetype)allocWithZone:(struct _NSZone *)zone    {
//    static id instance;
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{
//        instance = [[self alloc] init];
//    });
//    
//    return instance;
//}

+ (UIImage *)imageWithColor:(UIColor *)color AndSize:(CGSize)size AndCornerRadius:(CGFloat)radius  {
    CGRect rect = CGRectMake(0.0f, 0.0f, size.width, size.height);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
//    CGContextSetRGBStrokeColor(context,1,0,0,1.0);//画笔线的颜色
//    CGContextSetLineWidth(context, 1.0);//线的宽度
    CGContextSetFillColorWithColor(context, color.CGColor);;//设置填充颜色
    
//    // Add a clip before drawing anything, in the shape of an rounded rect
//    [[UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:radius] addClip];
    
    /*画圆角矩形*/
    float fw = size.width;
    float fh = size.height;
    
    CGContextMoveToPoint(context, fw, fh-10);  // 开始坐标右边开始
    CGContextAddArcToPoint(context, fw, fh, 10, fh, radius);  // 右下角角度
    CGContextAddArcToPoint(context, 0, fh, 0, 10, radius); // 左下角角度
    CGContextAddArcToPoint(context, 0, 0, fw-10, 0, radius); // 左上角
    CGContextAddArcToPoint(context, fw, 0, fw, fh-10, radius); // 右上角
    CGContextClosePath(context);
//    CGContextDrawPath(context, kCGPathFillStroke); //根据坐标绘制路径
    
    CGContextDrawPath(context, kCGPathFill);//绘制填充
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

+ (NSObject *)deepCopy:(NSObject<NSCoding> *)object {
    NSData * tempArchive = [NSKeyedArchiver archivedDataWithRootObject:object];
    return [NSKeyedUnarchiver unarchiveObjectWithData:tempArchive];
}

@end
