//
//  UIButton+ZQColorButton.m
//  NetworkSession
//
//  Created by zhangqiang on 16/3/9.
//  Copyright © 2016年 zhangqiang. All rights reserved.
//

#import "UIButton+ZQColorButton.h"

@implementation UIButton (ZQColorButton)
+ (instancetype)buttonWithNormalColor:(UIColor *)normalColor SelectedColor:(UIColor *)selectedColor frame:(CGRect)frame {
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = frame;
    
    //normal state color
    UIImage * normalImage = [EJUtils imageWithColor:normalColor AndSize:frame.size AndCornerRadius:5];
    [btn setBackgroundImage:normalImage forState:UIControlStateNormal];
    
    [btn setTitleColor:selectedColor forState:UIControlStateNormal];
    
    //selected state color
    UIImage * selectedImage = [EJUtils imageWithColor:selectedColor AndSize:frame.size AndCornerRadius:5];
    [btn setBackgroundImage:selectedImage forState:UIControlStateHighlighted];
    
    [btn setTitleColor:normalColor forState:UIControlStateHighlighted];
    
    return btn;
}
@end
