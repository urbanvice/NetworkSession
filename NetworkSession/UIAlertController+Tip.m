//
//  UIAlertController+Tip.m
//  NetworkSession
//
//  Created by zhangqiang on 16/3/10.
//  Copyright © 2016年 zhangqiang. All rights reserved.
//

#import "UIAlertController+Tip.h"

@implementation UIAlertController (Tip)

+ (void)alertTarget:(UIViewController *)target withTitle:(NSString *)title message:(NSString *)msg {
    UIAlertController *alertCtrl = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:[EJUtils txtWithId:5] style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        NSLog(@"Alert cancel");
    }];
    [alertCtrl addAction:confirmAction];
    
//    [alertCtrl showDetailViewController:<#(nonnull UIViewController *)#> sender:<#(nullable id)#>]
    [target presentViewController:alertCtrl animated:YES completion:nil];

}

@end
